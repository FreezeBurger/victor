# NGINX Configuration

## NGINX Configuration Path

```
/etc/nginx/nginx.conf
```

## Default Root Folder

Non-Dockerized Folder.

```
/srv/
```

Linux restart Nginx webserver, run:
# /etc/init.d/nginx restart

systemctl start nginx

OR
# /etc/init.d/nginx reload