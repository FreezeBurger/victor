
## Concourse

```sh
    HOST_PORT=22021 docker-compose  -f ./concourse-docker-compose.yml  up 
```

## Verdaccio

```sh
    HOST_PORT=22022 VERDACCIO_PORT=4873 docker-compose  -f ./verdaccio-docker-compose.yml  up 
```