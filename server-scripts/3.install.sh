#!/bin/sh

echo "\033[31m INITIAL: Installing curl \e[0m"
sudo apt update
sudo apt-get install curl
sudo apt update
echo 

echo "\033[31m 1/x: Installing Docker And Docker Compose \e[0m"
sudo apt update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo apt update
echo 

echo "\033[31m 2/x: NGINX Configuration \e[0m"
sudo apt update
sudo apt install nginx-extras
sudo apt update
sudo mkdir -p /srv/nginx/www/code.decryptage.net/
sudo systemctl enable nginx
sudo apt update
echo 


echo "\033[31m 2/x: Configuring Proxy \e[0m"
sudo apt update
sudo ufw allow OpenSSH
sudo ufw allow 80/tcp
sudo ufw allow http/tcp
sudo ufw allow 'Nginx HTTP'
sudo ufw allow 'Nginx HTTPS'
sudo ufw enable
sudo apt update
echo 

echo "\033[31m 2/x: Installing Certbot \e[0m"
sudo apt update
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot python-certbot-nginx
sudo certbot --nginx
sudo certbot renew --dry-run
sudo apt update
echo 

