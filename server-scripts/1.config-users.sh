#!/bin/sh
USERNAME="victor"
IP_ADDRESS="213.251.187.2"

echo "\033[31m 1/3 : Creating Victor User\e[0m"
sudo adduser $USERNAME
sudo usermod -aG sudo $USERNAME
apt update
echo

echo "\033[31m 2/3 : Update Public Key\e[0m"
echo "ssh-copy-id -i ~/.ssh/victor_rsa.pub $USERNAME@$IP_ADDRESS"
echo "ssh-copy-id -i ~/.ssh/victor_rsa.pub root@$IP_ADDRESS"
echo
echo

echo "\033[31m 3/3: Test SSH Connection \e[0m"



