# Bare Installation :

1. Start with Fresh Install
2. `Local` RESET.md
3. `Local` SSH.md
4. Connect and Follow SSH.md step 5 for `root`.
5. Copy `/victor/` under `/etc/victor/`
6. Run `/etc/server-scripts/1.config-users.sh`
7. Follow SSH.md step 5 for newly created users.
8. Run `/etc/victor/server-scripts/2.install.sh`
9. Run `/etc/victor/server-scripts/disable-password-auth.sh`

# Changes :

* sudo ufw allow OpenSSH
* sudo ufw enable
* ssh-keygen x_victor
* Removed Password Login

# TODO :

* System Health : https://askubuntu.com/questions/293426/system-monitoring-tools-for-ubuntu
* Docker Relaunch on Reboot

Check on :

* Latest Kubernetes 1.18 beta is now available for your laptop, NUC, cloud
   instance or Raspberry Pi, with automatic updates to the final GA release.

     sudo snap install microk8s --channel=1.18/beta --classic

* Multipass 1.1 adds proxy support for developers behind enterprise
   firewalls. Rapid prototyping for cloud operations just got easier.

     https://multipass.run/