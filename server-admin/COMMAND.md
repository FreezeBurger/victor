
# Command :

## Server : 

Restart : 

```
sudo reboot
```

## Uer Management : 

Add User : 
```
adduser user_name
```
Grant Admin Priviledge : 
```
usermod -aG sudo user_name
```

## Software Management :

Update system : 
```
sudo apt update
```
List Possible upgrades : 
```
apt list --upgradable
```

## Network :
View existing connection in the firewall : 
```
sudo ufw app list
```
Allow a connection via the firewall : 
```
ufw allow service_name
```
Open a specific port on the firewall : 
```
sudo ufw allow port/protocol
```
Check the status of the firewall execute : 
```
sudo ufw status
```
Enable the firewall :
```
sudo ufw enable
```