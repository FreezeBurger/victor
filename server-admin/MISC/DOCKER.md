# Docker

> https://docs.docker.com/install/linux/docker-ce/ubuntu/


## 1. Install Dependencies

```

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

```

## 2. Install Docker

Add Docker’s official GPG key:

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

```

Install the latest version of Docker CE and containerd

```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

https://medium.com/@hfally/a-gitlab-ci-config-to-deploy-to-your-server-via-ssh-43bf3cf93775

# Transfer
https://stackoverflow.com/questions/23935141/how-to-copy-docker-images-from-one-host-to-another-without-using-a-repository

# Deploying / Management

https://microk8s.io/docs/

## Private registry

https://dev.to/fuksito/how-to-setup-private-docker-registry-for-your-projects-to-save-money-1ed7

## Monitoring
https://medium.com/@karthi.net/top-6-gui-tools-for-managing-docker-environments-ee2d69ba5a4f

https://linuxhint.com/web_based_docker_monitoring_tools/

https://rancher.com/quick-start/