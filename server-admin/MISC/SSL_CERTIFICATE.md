# SSL_CERTIFICATE

> * https://letsencrypt.org/getting-started/
> * https://certbot.eff.org/instructions


  "certbot renew"

  # Wildcard

  letsencrypt certonly --manual --preferred-challenges dns --register -d code.decryptage.net -d *.code.decryptage.net