# UFW Proxy 

> https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw/

## Open HTTP Port 80

```
sudo ufw allow 80/tcp
sudo ufw allow http/tcp
```

